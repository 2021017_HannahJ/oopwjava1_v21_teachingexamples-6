package oojava1.m04.ex02_byvalue;

public class ByValueDemo {

	public static void main(String[] args) {
		int n = 10;
		System.out.printf("Value of variable                                : %d\n", n);
		incValue(n);
		System.out.printf("Value of variable outside method after increment : %d\n", n);
	}
	
	public static void incValue(int m) {
		m++;
		System.out.printf("Value of variable inside method after increment  : %d\n", m);
	}

}
