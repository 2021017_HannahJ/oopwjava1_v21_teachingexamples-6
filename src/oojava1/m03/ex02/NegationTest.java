package oojava1.m03.ex02;
/**
 * Demo of negation
 * @author jarih
 *
 */
public class NegationTest {
	public static void main(String[] args) {
		int i = 0;
		System.out.printf(" i==1 %s\n"  ,(  (i==1)  ) ? "Evaluates True" : "Evaluates False");
		System.out.printf("!(i==1) %s\n",( !(i==1)  ) ? "Evaluates True" : "Evaluates False");
		System.out.printf("i!=1 %s\n"    ,(  (i!=1)  ) ? "Evaluates True" : "Evaluates False");
		System.out.printf("!(i>0)==true) %s\n"    ,(  !(i>0)==true)   ? "Evaluates True" : "Evaluates False");
		System.out.printf("(i>0)!=true) %s\n"    ,(  (i>0)!=true)   ? "Evaluates True" : "Evaluates False");
	}

}
