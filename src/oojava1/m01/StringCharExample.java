package oojava1.m01;

public class StringCharExample {

	public static void main(String[] args) {
		char c = 'a';
		String s = "a";
		String aStr = Character.toString(c);
		System.out.println("A Character turned into a String: "+aStr);
	}

}
